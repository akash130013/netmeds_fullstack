import React from "react";
// import firebase from 'firebase/app'
// import 'firebase/auth';
import { withRouter } from "react-router";
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import history from '../helpers/history'
import { reduxForm, Field } from 'redux-form'

class Login extends React.Component {

  renderError = ({ error, touched }) => {
    return error && touched && <div className="ui error message">{error}</div>
  }

  // renderField = ({ input, label, meta }) => {

  //   return (
  //     <div className="field">
  //       <label>{label}</label>
  //       <input {...input} />
  //       <div>{this.renderError(meta)}</div>
  //     </div>
  //   )
  // }

  onSubmit = (formVal) => {

    console.log('form submited',formVal);
    return false;
    // this.props.onSubmit(formVal);

  }


  renderField = ({ input, label,name, meta,placeholder,icon,iconPosition,type }) => {
    console.log(12,input,meta);
    return (
      <>
      <Form.Input
        fluid
        icon={icon}
        iconPosition={iconPosition}
        placeholder={placeholder}
        name={name}
        type={type}
      />
      <div>{this.renderError(meta)}</div>
      </>
    )
  }


  render() {
    return (
      <div className="ui container">

        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' color='teal' textAlign='center'>
              Log-in to your account
           </Header>
            <Form size='large' onSubmit={this.props.handleSubmit(this.onSubmit)}>
              <Segment stacked>
               
                <Field 
                  fluid
                  icon='user'
                  iconPosition='left'
                  placeholder='E-mail address'
                  name="email"
                  type="email"
                  component={this.renderField}
                 
                />
                <Field 
                  fluid
                  icon='lock'
                  iconPosition='left'
                  placeholder='Password'
                  type="password"
                  component={this.renderField}
                  name="password"
                />

                <Button color='teal' fluid size='large'>
                  Login
              </Button>
              </Segment>
            </Form>
            <Message>
              <h6>UserId: abc@yopmail.com</h6>
              <h6>Password: 123456</h6>

            </Message>
          </Grid.Column>
        </Grid>

      </div>
    );
  }
}

// export default withRouter(Login);

const validate = (formVal) => {
  const errors = {}

  if (!formVal.email) {
    errors.email = 'This field is required'
  }

  if (!formVal.password) {
    errors.password = 'This field is required'
  }
  // console.log('11', errors);

  return errors
}

export default reduxForm({
  form: 'reduxLoginForm', // a unique identifier for this form
  validate,
})(Login)