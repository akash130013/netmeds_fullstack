import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import  productReducer from '../reducers/productReducer'


export default combineReducers({
   form: formReducer,
   products:productReducer,
})