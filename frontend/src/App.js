import React,{useEffect} from "react";
// components
import Navbar from "./common/Navbar";
import { Router, Route, Switch,Redirect } from 'react-router-dom'
import ProductContainer from "./components/ProductContainer";
import Login from './components/Login'
import {PrivateRoute} from './components/PrivateRoute'
import reducer from "./store/reducers"
import { Provider } from "react-redux"
import reduxThunk from 'redux-thunk'
import history from './helpers/history'
import 'semantic-ui-css/semantic.min.css'
//firebase
import withFirebaseAuth from 'react-with-firebase-auth'
import firebase from 'firebase/app'
import 'firebase/auth';
import firebaseConfig from './firebaseConfig';
import { createStore,applyMiddleware, compose } from "redux"

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(reduxThunk)
));


const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAppAuth = firebaseApp.auth();
const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider(),
};

function App(props) {
 

  useEffect(() => {
   if(!localStorage.getItem('accessToken')){
      history.push('/login');
   }
  }, [])


  return (
    
    <main>

      <Router history={history}>

        <Switch>

        <Provider store={store}>
       { localStorage.getItem("accessToken") && <Navbar  />}            
            <PrivateRoute exact path="/products" component={ProductContainer} />
            <Route path="/login" component={Login}  />
           
        </Provider>  
     
        </Switch>
      </Router>

    </main>
  );
}

export default withFirebaseAuth({
  providers,
  firebaseAppAuth,
})(App);